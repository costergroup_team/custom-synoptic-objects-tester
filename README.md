[![Netlify Status](https://api.netlify.com/api/v1/badges/37e4568c-e4d5-467a-a4a3-a60e36625be2/deploy-status)](https://app.netlify.com/sites/synoptic-objects-tester/deploys)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

## About this project

This project aims to create reusable/customizable widgets in a react application, 
these widgets will be created after the web application and get hot-loaded into a running
application.

To achieve this we needed something more than standard svgs and here is an example of what we came up with:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" 
    xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="76px" 
    height="68px" viewBox="0 0 76 68" enable-background="new 0 0 76 68" xml:space="preserve">
	<params>
		<param id="percent" type="animated" defaultValueForRendering="100" />
		<param id="stroke" type="static" defaultValueForRendering="#33BCED" />
		<param id="percentinverted" type="internal" defaultValueForRendering="72" />
	</params>
	<g>
		<text transform="matrix(1 0 0 1 16.9902 43.4141)" fill="#2EA2DC" 
      font-family="Arial" font-weight="bold" font-size="18">
			param(percent)%
		</text>
	</g>
	<path stroke-dashoffset="param(percentinverted)" stroke-dasharray="180" 
    fill="none" stroke="param(stroke)" stroke-width="5" 
    stroke-miterlimit="10" d="M15.339,65.291
    c-7.667-6.452-12.54-16.119-12.54-26.924c0-19.425,15.746-35.171,35.17-35.171c19.423,0,35.17,15.746,35.17,35.171
    c0,10.805-4.873,20.472-12.54,26.924" />
	<script type="text/javascript">
		if(this['percent'] < 50)
		{
			this['percent'] = 50
		}
		this['percentinverted'] = 180 - this['percent']/100*180;
	</script>
</svg>
```

let's dissect it

### Parameters definition

```xml
<params>
	<param id="percent" type="animated" defaultValueForRendering="100" />
	<param id="stroke" type="static" uiType="color" defaultValueForRendering="#33BCED" />
	<param id="percentinverted" type="internal" defaultValueForRendering="72" />
</params>
```
This section defines the values used inside the svg, and is used by our 
CAD software to create a configuration menu for the widget the same way as it
used in this website to create the menu

Every entry defines a value that will be passed onto the svg and either replaced inside it
or used in the script section

#### Parameters types

|     Type    | Description                |
| :---------  | :------------------------- |
|  `animated` | When this value change at runtime it will be tweened |
|   `fixed`   | When this value change at runtime it will not be tweened  |
|   `static`  | Values that won't change at runtime |
|  `internal` | Values use internally that should not be shown in a menu orparametrized<br />  but need to be given a default value to be able to render a preview of<br /> the svg in the CAD software |

#### Parameters uiTypes

|     Type    | Description                |
| :---------  | :------------------------- |
|   `color`   | Will use a color picker in the menu |
|  `boolean`  | will use a checkbox in the menu  |
|   `text`    | will use a text input in the menu |

### Graphic section
```xml
<g>
	<text transform="matrix(1 0 0 1 16.9902 43.4141)" fill="#2EA2DC" 
    font-family="Arial" font-weight="bold" font-size="18">
    param(percent)%
  </text>
</g>
<path stroke-dashoffset="param(percentinverted)" stroke-dasharray="180" 
  fill="none" stroke="param(stroke)" stroke-width="5" 
  stroke-miterlimit="10" d="M15.339,65.291
  c-7.667-6.452-12.54-16.119-12.54-26.924c0-19.425,15.746-35.171,35.17-35.171c19.423,0,35.17,15.746,35.17,35.171
  c0,10.805-4.873,20.472-12.54,26.924" />
```

The graphic section is standard svg once the values "param(**parameterName**)" gets replaced

### Script section
```xml
<script type="text/javascript">
  if(this['percent'] < 50)
  {
    this['percent'] = 50
  }
  this['percentinverted'] = 180 - this['percent']/100*180;
</script>
```
Inside the script section the **this** object contains all the static/fixed parameters runtime values,
 and the static parameters values.\
Internal values are evaluated and added to **this** within the script