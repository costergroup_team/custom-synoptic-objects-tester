/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from "react";
import "./App.css";
import SVGElement from "./components/SVGElement";
import { useDrop, useMethods } from "react-use";
import ErrorBoundary from "./components/ErrorBonduary";
import Menu from "./components/Menu";
import bitbucket from "./bitbucketb.svg";

interface IAppState {
  init: boolean;
  svg: string;
  animatedSvgParameters: IObj;
  fixedSvgParameters: IObj;
  staticSvgParameters: IObjStr;
  xml: Document | null;
}

const initialState: IAppState = {
  init: false,
  svg: "",
  animatedSvgParameters: {},
  fixedSvgParameters: {},
  staticSvgParameters: {},
  xml: null,
};

function createMethods(state: IAppState) {
  return {
    setSvg(svg: string) {
      return {
        init: false,
        svg,
        animatedSvgParameters: {},
        fixedSvgParameters: {},
        staticSvgParameters: {},
        xml: null,
      };
    },
    setanimatedSvgParameters({ key, value }: { key: string; value: number }) {
      return {
        ...state,
        animatedSvgParameters: { ...state.animatedSvgParameters, [key]: value },
      };
    },
    setfixedSvgParameters({ key, value }: { key: string; value: number }) {
      return {
        ...state,
        fixedSvgParameters: { ...state.fixedSvgParameters, [key]: value },
      };
    },
    setstaticSvgParameters({ key, value }: { key: string; value: string }) {
      return {
        ...state,
        staticSvgParameters: { ...state.staticSvgParameters, [key]: value },
      };
    },
    Initilize({
      newanimatedSvgParameters,
      newfixedSvgParameters,
      newstaticSvgParameters,
      xml,
    }: {
      newanimatedSvgParameters: IObj;
      newfixedSvgParameters: IObj;
      newstaticSvgParameters: IObjStr;
      xml: Document;
    }) {
      return {
        ...state,
        init: true,
        animatedSvgParameters: newanimatedSvgParameters,
        fixedSvgParameters: newfixedSvgParameters,
        staticSvgParameters: newstaticSvgParameters,
        xml,
      };
    },
  };
}
function App() {
  // const [animatedSvgParameters, setanimatedSvgParameters] = useMap<IObj>({});
  // const [fixedSvgParameters, setfixedSvgParameters] = useMap<IObj>({});
  // const [staticSvgParameters, setstaticSvgParameters] = useMap<IObjStr>({});
  const [
    {
      xml,
      svg,
      init,
      animatedSvgParameters,
      fixedSvgParameters,
      staticSvgParameters,
    },
    methods,
  ] = useMethods(createMethods, initialState);

  useDrop({
    onFiles: (files) => {
      const reader = new FileReader();
      function handleFileRead(e: ProgressEvent<FileReader>) {
        methods.setSvg(e.target?.result?.toString());
      }
      reader.onload = handleFileRead;
      reader.readAsText(files[0]);
    },
    onUri: (uri) => console.log("uri", uri),
    onText: (text) => console.log("text", text),
  });

  useEffect(() => {
    if (svg.length > 0 && !init) {
      let svgXml = new window.DOMParser().parseFromString(svg, "text/xml");
      let newanimatedSvgParameters: IObj = {};
      let newfixedSvgParameters: IObj = {};
      let newstaticSvgParameters: IObjStr = {};
      if (svgXml.getElementsByTagName("params").length > 0) {
        for (let node of svgXml.getElementsByTagName("params")[0].children) {
          if (node.getAttribute("type") === "animated") {
            newanimatedSvgParameters[node.getAttribute("id")!] = parseInt(
              node.getAttribute("defaultValueForRendering")!
            );
          } else if (node.getAttribute("type") === "fixed") {
            newfixedSvgParameters[node.getAttribute("id")!] = parseInt(
              node.getAttribute("defaultValueForRendering")!
            );
          } else if (node.getAttribute("type") === "static") {
            newstaticSvgParameters[node.getAttribute("id")!] =
              node.getAttribute("defaultValueForRendering")!;
          }
        }
      }
      methods.Initilize({
        newanimatedSvgParameters,
        newfixedSvgParameters,
        newstaticSvgParameters,
        xml: svgXml,
      });
    }
  }, [svg, init]);

  return (
    <div className="App">
      <header className="App-header">
        {(svg && init && xml !== null && (
          <>
            <ErrorBoundary>
              <SVGElement
                data={{
                  svg: svg,
                  animatedSvgParameters: animatedSvgParameters,
                  fixedSvgParameters: fixedSvgParameters,
                  staticSvgParameters: staticSvgParameters,
                }}
              />
            </ErrorBoundary>
            <Menu
              data={{
                svg: svg,
                animatedSvgParameters: animatedSvgParameters,
                fixedSvgParameters: fixedSvgParameters,
                staticSvgParameters: staticSvgParameters,
              }}
              xml={xml}
              setanimatedSvgParameters={methods.setanimatedSvgParameters}
              setfixedSvgParameters={methods.setfixedSvgParameters}
              setstaticSvgParameters={methods.setstaticSvgParameters}
            />
          </>
        )) || <>Drop an svg file here</>}
        <br></br> <br></br>
        <a
          className="App-link"
          href="https://bitbucket.org/costergroup_team/custom-synoptic-objects-tester"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={bitbucket} className="App-logo" alt="logo" />
          Repository
        </a>
      </header>
    </div>
  );
}

export default App;
