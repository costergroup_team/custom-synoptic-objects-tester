/* eslint-disable react-hooks/rules-of-hooks */
import React, { memo, useState } from "react";
import { useDebounce } from "react-use";

export interface IMenuProps {
  data: svgObjectElementoSvg;
  setanimatedSvgParameters: (...payload: any) => void;
  setfixedSvgParameters: (...payload: any) => void;
  setstaticSvgParameters: (...payload: any) => void;
  xml: Document;
}
const DEBOUNCE_TIMER = 1000;

function Menu({
  data: { animatedSvgParameters, fixedSvgParameters, staticSvgParameters },
  setanimatedSvgParameters,
  setfixedSvgParameters,
  setstaticSvgParameters,
  xml,
}: IMenuProps): JSX.Element {
  return (
    <form>
      {Object.keys(animatedSvgParameters).map((key) => {
        const [val, setVal] = useState(animatedSvgParameters[key].toString());

        useDebounce(
          () => {
            setanimatedSvgParameters({ key: key, value: parseInt(val) });
          },
          DEBOUNCE_TIMER,
          [val]
        );
        return (
          <div key={key} className="row">
            <label htmlFor={key}>{key}</label>
            <input
              id={key}
              name={key}
              value={val}
              onChange={(e) => setVal(e.target.value)}
              type="text"
            />
          </div>
        );
      })}
      {Object.keys(fixedSvgParameters).map((key) => {
        const [val, setVal] = useState(fixedSvgParameters[key].toString());

        useDebounce(
          () => {
            setfixedSvgParameters({ key: key, value: parseInt(val) });
          },
          DEBOUNCE_TIMER,
          [val]
        );
        return (
          <div key={key} className="row">
            <label htmlFor={key}>{key}</label>
            <input
              id={key}
              name={key}
              value={val}
              onChange={(e) => setVal(e.target.value)}
              type="text"
            />
          </div>
        );
      })}
      {Object.keys(staticSvgParameters).map((key) => {
        const [val, setVal] = useState<string>(staticSvgParameters[key].toString());

        useDebounce(
          () => {
            setstaticSvgParameters({ key: key, value: val });
          },
          DEBOUNCE_TIMER,
          [val]
        );
        let uitype = [...xml.getElementsByTagName("params")[0].children]
          .find(
            (node) =>
              node.getAttribute("type") === "static" &&
              node.getAttribute("id") === key
          )
          ?.getAttribute("uiType") as uiTypes | undefined;
        switch (uitype) {
          case "boolean":
            return (
              <div key={key} className="row">
                <label htmlFor={key}>{key}</label>
                <input
                  id={key}
                  name={key}
                  checked={val.toLowerCase() === 'true'}
                  onChange={(e) => setVal(e.target.checked.toString())}
                  type="checkbox"
                />
              </div>
            );
          case "color":
            return (
              <div key={key} className="row">
                <label htmlFor={key}>{key}</label>
                <input
                  id={key}
                  name={key}
                  value={val}
                  onChange={(e) => setVal(e.target.value)}
                  type="color"
                />
              </div>
            );
          default:
            return (
              <div key={key} className="row">
                <label htmlFor={key}>{key}</label>
                <input
                  id={key}
                  name={key}
                  value={val}
                  onChange={(e) => setVal(e.target.value)}
                  type="text"
                />
              </div>
            );
        }
      })}
    </form>
  );
}

export default memo(Menu);
