interface IObj {
  [key: string]: number;
}
interface IObjStr {
  [key: string]: string;
}

type uiTypes = "color"|"boolean"
interface svgObjectElementoSvg {
  // coordinates: coordinatesSinottico
  // type: 'ElementoSvg'
  // hidden: boolean
  // file: string
  animatedSvgParameters: IObj;
  fixedSvgParameters: IObj;
  staticSvgParameters: IObjStr;
  svg: string;
}

interface coordinatesSinottico {
  x: number;
  y: number;
  w?: number;
  h?: number;
}
