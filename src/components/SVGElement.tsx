/* eslint-disable react-hooks/rules-of-hooks */
import React, { useRef, useState } from "react";
import { useToggle, useTween } from "react-use";
import { useDeepCompareEffect } from "react-use";

// type ISvgParameters = { [key: string]: number };
interface IPropsSVGElement {
  data: svgObjectElementoSvg;
}
/** se refreshato troppo velocemente useTween inizia a comportarsi in modo strano */
function SVGElement({
  data: {
    // coordinates,
    // file,
    // hidden,
    animatedSvgParameters,
    fixedSvgParameters,
    staticSvgParameters,
    svg,
  },
}: IPropsSVGElement): JSX.Element {
  const svgModule = svg;
  const [tweentime, settweentime] = useState(0);
  const valuetween = useTween("inCirc", tweentime, 0);
  const lastValues = useRef({
    ...animatedSvgParameters,
    ...fixedSvgParameters,
    ...staticSvgParameters,
  });
  const [toRedraw, refresh] = useToggle(true);

  useDeepCompareEffect(() => {
    if (tweentime === 500) {
      settweentime(501);
    } else {
      settweentime(500);
    }
  }, [animatedSvgParameters]);

  useDeepCompareEffect(() => {
    refresh();
  }, [fixedSvgParameters, staticSvgParameters]);

  const renderSvg = React.useMemo(() => {
    if (svgModule === undefined) {
      return null;
    }

    let augmentedSvg: string = svgModule;
    lastValues.current = {
      ...lastValues.current,
      ...fixedSvgParameters,
      ...staticSvgParameters,
    };
    //animated parameters
    for (const key of Object.keys(animatedSvgParameters)) {
      let lastValue = lastValues.current[key];
      let curValue = animatedSvgParameters[key];
      if (typeof lastValue === "number" && typeof curValue === "number") {
        const value = curValue - (curValue - lastValue) * (1 - valuetween);
        lastValues.current[key] = parseFloat(value.toFixed(2));
      }
      else if (typeof lastValue === "boolean" && typeof curValue === "boolean"){
        lastValues.current[key] = curValue
      }
    }

    if (
      svgModule !== undefined &&
      svgModule.includes('<script type="text/javascript">')
    ) {
      const scriptLines = svgModule
        .split('<script type="text/javascript">')[1]
        .split("</script>")[0];

      function exec(str: string) {
        // eslint-disable-next-line no-eval
        return eval('(function() {' + str + '}.call(lastValues.current))');
      }
      exec( scriptLines);
    }

    // const toReplace = {
    //   ...lastValues.current,
    //   ...fixedSvgParameters,
    //   ...staticSvgParameters,
    // };
    for (const key of Object.keys(lastValues.current)) {
      augmentedSvg = augmentedSvg.replaceAll(
        `param(${key})`,
        lastValues.current[key].toString()
      );
    }

    return <div dangerouslySetInnerHTML={{ __html: augmentedSvg }} />;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [valuetween, toRedraw]);

  return <>{renderSvg}</>;
}
export default SVGElement;
